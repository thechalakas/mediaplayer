package com.thechalakas.jay.mediaplayer;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity
{

    int position=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //video player
        final VideoView videoView = (VideoView) findViewById(R.id.videoView);
        //media controls
        MediaController mediaController = new MediaController(MainActivity.this);

        Log.i("MainActivity","onCreate - reached");

        try
        {
            //set the media controller in the VideoView
            //myVideoView.setMediaController(mediaControls);
            videoView.setMediaController(mediaController);

            //set the uri of the video to be played
            //myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kitkat));
            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.cateatsbiscuit));
            Log.i("MainActivity","Alright file looks okay");
        }
        catch (Exception e)
        {
            //Log.e("Error", e.getMessage());
            //e.printStackTrace();
            Log.i("MainActivity","Alright file DID NOT WORK");
        }

        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer)
            {
                Log.i("MainActivity","setOnPreparedListener - reached");

                videoView.seekTo(position);
                if(position==0)
                {
                    videoView.start();
                }
                else
                {
                    videoView.pause();
                }
            }
        });


    }
}
